import _ from 'lodash';

// styling bootstrap
import 'bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";

// vue core
import Vue from 'vue';
import VueRouter from 'vue-router';

// Set Pageing Page
import home from '@/src/page/home';


Vue.use(VueRouter);

export default new VueRouter({
    routes:[
        {
            path:'/',
            name:'home',
            component:home
        }
    ]
});

